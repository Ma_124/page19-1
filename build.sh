#!/bin/sh

# rm -r public
go get github.com/valyala/quicktemplate/... gopkg.in/yaml.v2
mkdir -p public
qtc -dir="templates"
cp assets/** public
go run main.go

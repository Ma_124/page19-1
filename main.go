package main

import (
	"io"
	"io/ioutil"
	"os"

	"./templates"
	"gopkg.in/yaml.v2"
)

//go:generate ./build.sh

func main() {
	compileTemplate(&templates.IndexParams{}, "index", templates.WriteIndex)
}

func compileTemplate(params interface{}, name string, fun func(io.Writer, interface{})) {
	data, err := ioutil.ReadFile(name + ".yaml")
	if err != nil {
		panic(err)
	}

	err = yaml.Unmarshal(data, params)
	if err != nil {
		panic(err)
	}

	f, err := os.Create("public/" + name + ".html")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	fun(f, params)
}

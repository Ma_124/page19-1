package templates

import (
	"io"
	qt "github.com/valyala/quicktemplate"
)

func writeunesc(iow io.Writer, s string) {
	qw := qt.AcquireWriter(iow)
	streamunesc(qw, s)
	qt.ReleaseWriter(qw)
}

func unesc(s string) string {
	qb := qt.AcquireByteBuffer()
	writeunesc(qb, s)
	qs := string(qb.B)
	qt.ReleaseByteBuffer(qb)
	return qs
}

func streamunesc(qw *qt.Writer, s string) {
	qw.N().S(s)
}

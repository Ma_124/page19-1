package templates

type LsItem struct {
	Time string
	Name string
	Href string
}

type IndexParams struct {
	Projects []LsItem
	Blogs []LsItem
	FavLangs []string `yaml:"langs"`
}

{% package templates %}

{% import (
    "time"
) %}

{% func head(title string) %}
<meta charset="UTF-8" />
<title>{%s title %}</title>
<link rel="stylesheet" href="style.css" />
<link rel="stylesheet" href="https://ma124.js.org/style.css" />
<script src="bundle.js"></script>
{% endfunc %}

{% code
%}

{% stripspace %}
{% func Index(paramz interface{}) %}
{% code
params := paramz.(*IndexParams)
%}
<!DOCTYPE html>
<html>
    <head>
        {%= head("Ma_124") %}
        <script>
            webshell.history = [
                "cat copyright.textile",
                "cat fav_langs.md",
                "ls -o --recent blog/",
                "ls -o src/ma_124/",
                "cat navbar.html",
            ];
            webshell.line = 6;
            webshell.fs = {
                "navbar.html": "{%=j indexNavbar() %}",
                "fav_langs.md": "{%=j indexFavLangs(params) %}",
                "copyright.textile": "{%=j indexCopyright() %}",
                "src": [],
                "src/ma_124": [],
                "blog": [],
            };
            webshell.init();
        </script>
    </head>
    <body>
        <div id="user-cmds">
            {%= command("cat navbar.html", false) %}
            {%= indexNavbar() %}
            <br />
            {%= cmd("ls -o src/ma_124/") %}
            {%= ls(params.Projects) %}
            {%= cmd("ls -o --recent blog/") %}
            {%= ls(params.Blogs) %}
            {%= cmd("cat fav_langs.md") %}
            {%= indexFavLangs(params) %}
            {%= cmd("cat copyright.textile") %}
            {%= indexCopyright() %}
            {%= cmd("<span id=\"current-cmd\"></span>") %}
        </div>
    </body>
</html>
{% endfunc %}
{% endstripspace %}

{% stripspace %}
{% func ls(its []LsItem) %}
<ol class="ls-items">
    {% for _, it := range its %}
    <li>
        <a href="{%s it.Href %}">
            <nobr>drw-r--r--</nobr>{% space %}
            ma_124{% space %}
            <nobr>{%code
            t, err := time.Parse("2006-01-02", it.Time)
            if err != nil {
                panic(err)
            }

            s := t.Format("Jan 02, 2006")
            %}</nobr>{% space %}{%s s %}{% space %}
            <nobr>{%s it.Name %}</nobr>
        </a>
    </li>
    {% endfor %}
</ol><br />
{% endfunc %}
{% endstripspace %}

{% stripspace %}
{% func indexNavbar() %}
<nav>
    {%= indexHtmlLink("Blog", "/blog", "") %}<br />
    {%= indexHtmlLink(indexHtml("img", [][]string{{"src", "tanuki.png"}}, ""), "https://gitlab.com/Ma_124", "gitlab.com/Ma_124") %}<br />
    {%= indexHtmlLink(indexHtml("img", [][]string{{"src", "octocat.png"}}, ""), "https://github.com/Ma124", "github.com/Ma124") %}<br />
</nav>
<br />
<br />
{% endfunc %}
{% endstripspace %}

{% stripspace %}
{% func indexHtml(tag string, attrs [][]string, content string) %}
&lt;
    <span class="xml-tag">{%s tag %}</span>
    {% for _, attr := range attrs %}
            {% space %}{%s attr[0] %}=<span class="xml-string">"{%s attr[1] %}"</span>
    {% endfor %}
    {% if content == "" %}
        {% space %}/&gt;
    {% else %}
        &gt;{%= unesc(content) %}&lt;<span class="xml-tag">{%s tag %}</span>&gt;
    {% endif %}
{% endfunc %}
{% endstripspace %}

{% stripspace %}
{% func indexFavLangs(params *IndexParams) %}
<span class="md-heading">
    Favourite Languages<br />
    ===================<br />
</span>
<br />
<ol class="md-list">
    {% for i, it := range params.FavLangs %}
        <li><span class="md-list-item-bullet">{%d i+1 %}.</span>{% space %}<span>{%s it %}</span></li>
    {% endfor %}
</ol>
<br />
{% endfunc %}
{% endstripspace %}

{% stripspace %}
{% func indexCopyright() %}
<a href="https://ma124.js.org/l/AGPL_full/~/2018">
    <span class="textile-lnk-txt">"Copyright (C) 2018 Ma_124"</span>:<span class="textile-lnk-url">/license.html</span>
</a><br />
<br />
{% endfunc %}
{% endstripspace %}

{% func cmd(cmd string) %}{%= command(cmd, true) %}{% endfunc %}

{% func command(cmd string, br bool) %}<nobr>ma@ma124.js.org: ~$ {%s= cmd %}</nobr>{% if br %}<br />{% endif %}{% endfunc %}

{% func indexHtmlLink(text, href, visHref string) %}
{% if visHref != "" %}
<a href="{%s href %}">{%= indexHtml("a", [][]string{{"href", visHref}}, text) %}</a>
{% else %}
<a href="{%s href %}">{%= indexHtml("a", [][]string{}, text) %}</a>
{% endif %}
{% endfunc %}

